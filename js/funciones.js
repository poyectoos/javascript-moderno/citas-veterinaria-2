import Citas from "./classes/Citas.js";
import UI from "./classes/UI.js";

import {
  formulario,
  mascota,
  propietario,
  telefono,
  fecha,
  hora,
  sintomas,
} from "./selectores.js";

const citas = new Citas();
const ui = new UI();

let editando = false;

const cita = {
  mascota: "",
  propietario: "",
  telefono: "",
  fecha: "",
  hora: "",
  sintomas: "",
};

export function datosCita(e) {
  cita[e.target.name] = e.target.value.trim();
}

export function nuevaCita(e) {
  e.preventDefault();
  const { mascota, propietario, telefono, fecha, hora, sintomas } = cita;

  if (
    mascota === "" ||
    propietario === "" ||
    telefono === "" ||
    fecha === "" ||
    hora === "" ||
    sintomas === ""
  ) {
    ui.alerta("Todos los campos son obligatorios", "error");
    return;
  }

  if (editando) {
    citas.editarCita({ ...cita });

    ui.alerta("Cita actualizada correctamente", "info");
  } else {
    const nuevaCita = {
      id: Date.now(),
      ...cita,
    };

    citas.agregarCita(nuevaCita);

    ui.alerta("Cita agregada correctamente", "success");
  }
  ui.mostrarCitas(citas);

  formulario.reset();
  reiniciarCita();
}
export function reiniciarCita() {
  cita.mascota = "";
  cita.propietario = "";
  cita.telefono = "";
  cita.fecha = "";
  cita.hora = "";
  cita.sintomas = "";

  delete cita.id;

  formulario.querySelector('button[type="submit"]').textContent = "Crear cita";

  editando = false;
}
export function eliminarCita(id) {
  citas.eliminarCita(id);
  ui.mostrarCitas(citas);
  ui.alerta("La cita se elimino correctamente", "success");
}
export function editarCita(informacion) {
  mascota.value = informacion.mascota;
  propietario.value = informacion.propietario;
  telefono.value = informacion.telefono;
  fecha.value = informacion.fecha;
  hora.value = informacion.hora;
  sintomas.value = informacion.sintomas;

  cita.mascota = informacion.mascota;
  cita.propietario = informacion.propietario;
  cita.telefono = informacion.telefono;
  cita.fecha = informacion.fecha;
  cita.hora = informacion.hora;
  cita.sintomas = informacion.sintomas;
  cita.id = informacion.id;

  editando = true;

  formulario.querySelector('button[type="submit"]').textContent =
    "Guardar Cambios";
}

export const formulario = document.querySelector("#nueva-cita");
export const contenedorCitas = document.querySelector('#citas');

export const mascota = formulario.querySelector("#mascota");
export const propietario = formulario.querySelector("#propietario");
export const telefono = formulario.querySelector("#telefono");
export const fecha = formulario.querySelector("#fecha");
export const hora = formulario.querySelector("#hora");
export const sintomas = formulario.querySelector("#sintomas");
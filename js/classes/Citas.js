/*
==================================================
          Citas
==================================================
*/
class Citas {
  constructor() {
    this.citas = [];
  }
  agregarCita(cita) {
    this.citas = [...this.citas, cita];
  }
  eliminarCita(id) {
    this.citas = this.citas.filter(cita => cita.id !== id);
  }
  editarCita(citaEditada) {
    this.citas = this.citas.map(cita => cita.id === citaEditada.id ? citaEditada : cita); 
  }
}

export default Citas;
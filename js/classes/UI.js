import { eliminarCita, editarCita  } from "../funciones.js";
import { contenedorCitas } from "../selectores.js";
/*
==================================================
          UI
==================================================
*/
class UI {
  alerta(mensaje, tipo) {
    const alerta = document.createElement('div');
    alerta.textContent = mensaje;
    alerta.classList.add('text-center', 'alert', 'd-block', 'col-12');
    if (tipo === 'error') {
      alerta.classList.add('alert-danger');
    } else if (tipo === 'success') {
      alerta.classList.add('alert-success');
    } else if (tipo === 'info') {
      alerta.classList.add('alert-info');
    }
    const contenedor = document.querySelector('#alerta');
    contenedor.appendChild(alerta);

    setTimeout(() => {
      alerta.remove();
    }, 4000);
  }
  mostrarCitas({citas}) {
    this.limpiarCitas();
    citas.forEach(cita => {
      const { id, mascota, propietario, telefono, fecha, hora, sintomas } = cita;
      const div = document.createElement('div');
      div.classList.add('cita', 'p-3');
      div.dataset.id = id;

      const tMascota = document.createElement('h2');
      tMascota.classList.add('card-title', 'font-weight-bolder');
      tMascota.textContent = `${mascota}`;

      const tPropietario = document.createElement('p');
      tPropietario.innerHTML = `
        <span class="font-weight-bolder">Propietario: </span>${propietario}
      `;
      
      const tTelefono = document.createElement('p');
      tTelefono.innerHTML = `
        <span class="font-weight-bolder">Telefono: </span>${telefono}
      `;
      
      const tFecha = document.createElement('p');
      tFecha.innerHTML = `
        <span class="font-weight-bolder">Fecha: </span>${fecha}
      `;

      const tHora = document.createElement('p');
      tHora.innerHTML = `
        <span class="font-weight-bolder">Hora: </span>${hora}
      `;
      
      const tSintomas = document.createElement('p');
      tSintomas.innerHTML = `
        <span class="font-weight-bolder">Sintomas: </span>${sintomas}
      `;

      const eliminar = document.createElement('button');
      eliminar.classList.add('btn', 'btn-danger', 'm-2');
      eliminar.innerHTML = 'Eliminar <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>';
      eliminar.onclick = () => eliminarCita(id);

      
      const editar = document.createElement('button');
      editar.classList.add('btn', 'btn-info', 'm-2');
      editar.innerHTML = 'Editar <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"></path></svg>';
      editar.onclick = () => editarCita(cita);

      div.appendChild(tMascota);
      div.appendChild(tPropietario);
      div.appendChild(tTelefono);
      div.appendChild(tFecha);
      div.appendChild(tHora);
      div.appendChild(tSintomas);
      div.appendChild(eliminar);
      div.appendChild(editar);
      contenedorCitas.appendChild(div);
    });
  }
  limpiarCitas() {
    while( contenedorCitas.firstChild ) {
      contenedorCitas.removeChild(contenedorCitas.firstChild);
    }
  }
}

export default UI;
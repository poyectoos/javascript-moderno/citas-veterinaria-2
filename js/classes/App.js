import { datosCita, nuevaCita } from "../funciones.js";
import {
  mascota,
  propietario,
  telefono,
  fecha,
  hora,
  sintomas,
  formulario,
} from "../selectores.js";

class App {
  constructor() {
    this.initApp();
  }
  initApp() {
    mascota.addEventListener("input", datosCita);
    propietario.addEventListener("input", datosCita);
    telefono.addEventListener("input", datosCita);
    fecha.addEventListener("input", datosCita);
    hora.addEventListener("input", datosCita);
    sintomas.addEventListener("input", datosCita);

    formulario.addEventListener("submit", nuevaCita);
  }
}

export default App;
